import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_index/widgets/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class ThirdTab extends StatelessWidget {
  const ThirdTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.050,
          ),
          //Title
          SizedBox(
              height: MediaQuery.of(context).size.height * 0.10,
              width: MediaQuery.of(context).size.width * 0.60,
              child: AutoSizeText(
                "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter",
                style: GoogleFonts.lato(
                  color: HexColor("#4A5568"),
                  fontSize: 32,
                ),
                textAlign: TextAlign.center,
              )),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.070,
          ),
          // Service Container number 1
          Container(
            alignment: Alignment.center,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ServiceOneMobile(
                  number: 1,
                  title: "Erstellen dein Unternehmensprofil",
                ),
              ],
            ),
          ),
          // Service Container number 2
          ClipPath(
            clipper: MyServiceClipperMobile(),
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [
                    HexColor("#E6FFFA"),
                    HexColor("#EBF4FF"),
                  ],
                      stops: const [
                    0.0,
                    1.0
                  ],
                      begin: FractionalOffset.centerLeft,
                      end: FractionalOffset.centerRight,
                      tileMode: TileMode.repeated)),
              height: MediaQuery.of(context).size.height * 0.6,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ServiceTwoMobile(
                    image: "assets/images/third_tab_second_service.svg",
                    number: 2,
                    title: "Erhalte Vermittlungs- angebot von Arbeitgeber",
                  ),
                ],
              ),
            ),
          ),
          // // Service Container number 3
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.45,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ServiceThreeMobile(
                  image: "assets/images/third_tab_third_service.svg",
                  number: 3,
                  title: "Vermittlung nach Provision oder Stundenlohn",
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 150,
          )
        ],
      ),
    );
  }
}
