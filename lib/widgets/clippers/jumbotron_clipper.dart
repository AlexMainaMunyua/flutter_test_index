import 'package:flutter/material.dart';

class MyJumboTronClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0.0, size.height);

    var fistStart = Offset(size.width / 5, size.height);
    var firstEnd = Offset(size.width / 2.0, size.height - 50.0);
    path.quadraticBezierTo(
        fistStart.dx, fistStart.dy, firstEnd.dx, firstEnd.dy);

    var secondStart = Offset(size.width - (size.width / 3), size.height - 100);
    var secondEnd = Offset(size.width, size.height - 90.0);
    path.quadraticBezierTo(
        secondStart.dx, secondStart.dy, secondEnd.dx, secondEnd.dy);

    path.lineTo(size.width, 0);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
