import 'package:flutter/material.dart';

class MyServiceClipperMobile extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    // P1
    path.lineTo(0.0, size.height);

    // P2 TODO:
    // path.lineTo(size.width, size.height - 50);
    path.cubicTo(size.width / 3.4, size.height / 1.4, size.width / 1.5,
        size.height * 1.1, size.width, size.height - 80);

    // P3
    path.lineTo(size.width, 0.0);

    // P4 TODO:
    path.cubicTo(size.width / 1.4, size.height / 3.4, size.width / 3.4, 0.0,
        0.0, size.height / 5.0);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
