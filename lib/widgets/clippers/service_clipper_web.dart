import 'package:flutter/material.dart';

class MyServiceClipperWeb extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0.0, size.height);

    var fistStart = Offset(size.width / 5, size.height);
    var firstEnd = Offset(size.width / 3.0, size.height - 50.0);
    path.quadraticBezierTo(
        fistStart.dx, fistStart.dy, firstEnd.dx, firstEnd.dy);

    var secondStart =
        Offset(size.width - (size.width / 3.24), size.height - 30);
    var secondEnd = Offset(size.width, size.height - 150.0);
    path.quadraticBezierTo(
        secondStart.dx, secondStart.dy, secondEnd.dx, secondEnd.dy);

    path.lineTo(size.width, size.height - 350.0);

    var thirdStart = Offset(size.width - (size.width / 5), size.height - 450);
    var thirdEnd = Offset(size.width - (size.width / 3), size.height - 400.0);
    path.quadraticBezierTo(
        thirdStart.dx, thirdStart.dy, thirdEnd.dx, thirdEnd.dy);

    var fourthStart =
        Offset(size.width - (size.width / 2.0), size.height - 350);
    var fourthEnd = Offset(size.width / 5, size.height - 500);
    path.quadraticBezierTo(
        fourthStart.dx, fourthStart.dy, fourthEnd.dx, fourthEnd.dy);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
