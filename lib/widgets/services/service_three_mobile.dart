import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';

class ServiceThreeMobile extends StatelessWidget {
  ServiceThreeMobile({Key? key, this.number, this.title, required this.image})
      : super(key: key);
  int? number;
  String? title;
  String image;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.7,
      child: FittedBox(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.only(left: 40, right: 40),
                  child: Text(
                    "${number!}.",
                    style: GoogleFonts.lato(
                      color: HexColor("#718096"),
                      fontSize: 200,
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.11,
                  width: MediaQuery.of(context).size.height * 0.35,
                  alignment: Alignment.bottomLeft,
                  child: AutoSizeText(
                    title!,
                    style: GoogleFonts.lato(
                      color: HexColor("#718096"),
                      fontSize: 32,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 60,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.4,
              width: MediaQuery.of(context).size.width * 0.8,
              child: SvgPicture.asset(
                image,
                fit: BoxFit.fill,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
