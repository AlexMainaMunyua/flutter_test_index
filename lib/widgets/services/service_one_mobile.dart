import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class ServiceOneMobile extends StatelessWidget {
  ServiceOneMobile({Key? key, this.number, this.title}) : super(key: key);
  int? number;
  String? title;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // color: Colors.pink,
      width: MediaQuery.of(context).size.width * 0.9,
      child: FittedBox(
        child: Column(
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.25,
              width: MediaQuery.of(context).size.height * 0.40,
              child: SvgPicture.asset(
                'assets/images/service_image_one.svg',
                fit: BoxFit.fill,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.only(right: 25),
                  child: AutoSizeText(
                    "${number!}.",
                    style: GoogleFonts.lato(
                      color: HexColor("#718096"),
                      fontSize: 130,
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.11,
                  width: MediaQuery.of(context).size.height * 0.44,
                  alignment: Alignment.bottomLeft,
                  child: AutoSizeText(
                    title!,
                    style: GoogleFonts.lato(
                      color: HexColor("#718096"),
                      fontSize: 32,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
