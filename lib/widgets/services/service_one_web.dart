import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class ServiceOneWeb extends StatelessWidget {
  ServiceOneWeb({Key? key, this.number, this.title}) : super(key: key);
  int? number;
  String? title;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.5,
      width: MediaQuery.of(context).size.width * 0.7,
      child: FittedBox(
        child: Row(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(right: 25),
                  child: AutoSizeText(
                    "${number!}.",
                    style: GoogleFonts.lato(
                      color: HexColor("#718096"),
                      fontSize: 130,
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.11,
                  width: MediaQuery.of(context).size.height * 0.38,
                  alignment: Alignment.bottomLeft,
                  child: Text(
                    title!,
                    style: GoogleFonts.lato(
                      color: HexColor("#718096"),
                      fontSize: 32,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.25,
              width: MediaQuery.of(context).size.height * 0.25,
              child: SvgPicture.asset('assets/images/service_image_one.svg'),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.2,
            ),
          ],
        ),
      ),
    );
  }
}
