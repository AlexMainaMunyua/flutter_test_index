import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';

class ServiceTwoWeb extends StatelessWidget {
  ServiceTwoWeb({Key? key, this.number, this.title}) : super(key: key);
  int? number;
  String? title;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.6,
      height: MediaQuery.of(context).size.height * 0.45,
      child: FittedBox(
        child: Row(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.1,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.25,
              child: SvgPicture.asset('assets/images/service_image_two.svg'),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.only(left: 40, right: 40),
                  child: Text(
                    "${number!}.",
                    style: GoogleFonts.lato(
                      color: HexColor("#718096"),
                      fontSize: 130,
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.11,
                  width: MediaQuery.of(context).size.height * 0.44,
                  alignment: Alignment.bottomLeft,
                  child: AutoSizeText(
                    title!,
                    style: GoogleFonts.lato(
                      color: HexColor("#718096"),
                      fontSize: 32,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
