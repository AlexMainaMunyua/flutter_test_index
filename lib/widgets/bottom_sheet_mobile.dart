import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class BottomSheetMobile extends StatelessWidget {
  BottomSheetMobile({Key? key, this.onTap}) : super(key: key);
  VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(10), topLeft: Radius.circular(10))),
        onClosing: () {},
        builder: (context) {
          return Material(
            elevation: 10,
            child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
              ),
              height: MediaQuery.of(context).size.height * 0.15,
              child: Align(
                alignment: Alignment.center,
                child: GestureDetector(
                  onTap: onTap,
                  child: Container(
                    alignment: Alignment.center,
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width * 0.9,
                    decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10)),
                        gradient: LinearGradient(
                            colors: [
                              HexColor("#319795"),
                              HexColor("#3182CE"),
                            ],
                            stops: const [
                              0.0,
                              1.0
                            ],
                            begin: FractionalOffset.centerLeft,
                            end: FractionalOffset.centerRight,
                            tileMode: TileMode.repeated)),
                    child: Center(
                      child: AutoSizeText(
                        "Kostenlos Registrieren",
                        style: GoogleFonts.lato(
                            fontWeight: FontWeight.w500,
                            letterSpacing: 1,
                            color: HexColor("#E6FFFA"),
                            fontSize: 17),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        });
  }
}
