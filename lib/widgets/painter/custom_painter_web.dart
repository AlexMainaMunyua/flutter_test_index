import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:path_drawing/path_drawing.dart';

class CustomPainterWeb extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    double dashHeight = 5, dashSpace = 3, startY = 0;
    var paint = Paint();
    var path = Path();
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = 0.5;
    paint.color = HexColor("#718096");

    // First service curve
    path.moveTo(size.width / 5.0, size.height / 4.2);
    path.cubicTo(size.width / 5.0, size.height / 2.3, size.width / 2.0,
        size.height / 3.6, size.width / 2.0, size.height / 2.3);

    // First service arrow
    path.moveTo(size.width / 2.0, size.height / 2.29);
    path.lineTo(size.width / 1.96, size.height / 2.34);
    path.moveTo(size.width / 2.0, size.height / 2.29);
    path.lineTo(size.width / 2.04, size.height / 2.34);

    // Second service curve
    path.moveTo(size.width / 1.8, size.height / 1.8);
    path.cubicTo(size.width / 1.8, size.height / 1.3, size.width * 0.3,
        size.height * 0.6, size.width * 0.3, size.height / 1.3);

    // second service arrow
    path.moveTo(size.width * 0.30, size.height / 1.3);
    path.lineTo(size.width * 0.29, size.height / 1.32);
    path.moveTo(size.width * 0.299, size.height / 1.3);
    path.lineTo(size.width * 0.31, size.height / 1.32);

    canvas.drawPath(
        dashPath(
          path,
          dashArray: CircularIntervalList<double>(<double>[5.0, 2.5]),
        ),
        paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }
}
