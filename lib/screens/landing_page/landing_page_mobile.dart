import 'package:flutter/material.dart';
import 'package:flutter_test_index/components/components.dart';
import 'package:flutter_test_index/widgets/bottom_sheet_mobile.dart';

class LandingPageMobile extends StatefulWidget {
  const LandingPageMobile({Key? key}) : super(key: key);

  @override
  State<LandingPageMobile> createState() => _LandingPageMobileState();
}

class _LandingPageMobileState extends State<LandingPageMobile> {
  late ScrollController _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _scrollToTop() {
    _scrollController.animateTo(0,
        duration: const Duration(seconds: 3), curve: Curves.linear);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MenuBarMobile(),
      body: SingleChildScrollView(
        controller: _scrollController,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            JumbotronMobile(),
            SizedBox(
              height: 40,
            ),
            ServicesMobile(),
          ],
        ),
      ),
      bottomSheet: BottomSheetMobile(
        onTap: _scrollToTop,
      ),
    );
  }
}
