import 'package:flutter/material.dart';
import 'package:flutter_test_index/screens/landing_page/landing_page_mobile.dart';
import 'package:flutter_test_index/screens/landing_page/landing_page_web.dart';
import 'package:responsive_builder/responsive_builder.dart';

class LandingPageResponsive extends StatelessWidget {
  const LandingPageResponsive({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      mobile: (_) => const LandingPageMobile(),
      desktop: (_) => const LandingPageWeb(),
    );
  }
}
