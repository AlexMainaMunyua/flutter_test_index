import 'package:flutter/material.dart';

import '../../components/components.dart';

class LandingPageWeb extends StatelessWidget {
  const LandingPageWeb({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MenuBarWeb(),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            JumbotronWeb(),
            SizedBox(
              height: 40,
            ),
            ServicesWeb(),
          ],
        ),
      ),
    );
  }
}
