import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class MenuBarWeb extends StatelessWidget implements PreferredSizeWidget {
  const MenuBarWeb({Key? key}) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(60.0);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 5,
      borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(10.0),
          bottomRight: Radius.circular(10.0)),
      child: Column(
        children: [
          Container(
            height: 4.0,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [
                  HexColor("#319795"),
                  HexColor("#3182CE"),
                ],
                    stops: const [
                  0.0,
                  1.0
                ],
                    begin: FractionalOffset.centerLeft,
                    end: FractionalOffset.centerRight,
                    tileMode: TileMode.repeated)),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0, right: 8.0),
            child: Align(
                alignment: Alignment.centerRight,
                child: TextButton(
                    onPressed: () {},
                    child: Text(
                      "Login",
                      style: GoogleFonts.lato(
                          fontSize: 14, color: HexColor("#319795")),
                    ))),
          ),
        ],
      ),
    );
  }
}
