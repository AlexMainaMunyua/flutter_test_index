import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_index/widgets/tabs/first_tab.dart';
import 'package:flutter_test_index/widgets/tabs/second_tab.dart';
import 'package:flutter_test_index/widgets/tabs/third_tab.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

import '../../widgets/tabview/tab_view.dart';

class ServicesMobile extends StatefulWidget {
  const ServicesMobile({Key? key}) : super(key: key);

  @override
  State<ServicesMobile> createState() => _ServicesMobileState();
}

class _ServicesMobileState extends State<ServicesMobile>
    with TickerProviderStateMixin {
  late TabController _tabController;

  int _currentIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _tabController.addListener(_tabSelect);
  }

  void _tabSelect() {
    setState(() {
      _currentIndex = _tabController.index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 1.8,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(10.0)),
                border: Border.all(color: Colors.transparent)),
            child: TabBar(
              controller: _tabController,
              isScrollable: true,
              labelColor: HexColor("#E6FFFA"),
              unselectedLabelColor: HexColor("#319795"),
              indicator: BoxDecoration(
                color: HexColor("#81E6D9"),
                borderRadius: _currentIndex == 0
                    ? const BorderRadius.only(
                        topLeft: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0))
                    : _currentIndex == 2
                        ? const BorderRadius.only(
                            topRight: Radius.circular(10.0),
                            bottomRight: Radius.circular(10.0))
                        : const BorderRadius.all(
                            Radius.circular(0.0),
                          ),
              ),
              tabs: [
                Tab(
                  child: Container(
                    padding: const EdgeInsets.only(left: 40, right: 40),
                    child: Center(
                        child: AutoSizeText(
                      "Arbeitnehmer",
                      style: GoogleFonts.lato(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        // color: HexColor("#E6FFFA")
                      ),
                    )),
                  ),
                ),
                Tab(
                  child: Container(
                    padding: const EdgeInsets.only(left: 40, right: 40),
                    child: Center(
                        child: AutoSizeText(
                      "Arbeitgeber",
                      style: GoogleFonts.lato(
                          // color: HexColor("#319795"),
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    )),
                  ),
                ),
                Tab(
                  child: Container(
                    padding: const EdgeInsets.only(left: 40, right: 40),
                    child: Center(
                        child: AutoSizeText(
                      "Temporärbüro",
                      style: GoogleFonts.lato(
                          // color: HexColor("#319795"),
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    )),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: _tabController,
              children: const [FirstTab(), SecondTab(), ThirdTab()],
            ),
          ),
        ],
      ),
    );
  }
}
