import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_index/widgets/painter/custom_painter_web.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

import '../../widgets/widgets.dart';

class ServicesWeb extends StatefulWidget {
  const ServicesWeb({Key? key}) : super(key: key);

  @override
  State<ServicesWeb> createState() => _ServicesWebState();
}

class _ServicesWebState extends State<ServicesWeb> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        //const TabView
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.010,
        ),
        const TabViewWidget(),
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.050,
        ),
        //Title
        LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
          if (constraints.maxWidth < 1300) {
            return SizedBox(
                height: MediaQuery.of(context).size.height * 0.10,
                width: MediaQuery.of(context).size.width * 0.30,
                child: AutoSizeText(
                  "Drei einfache Schritte zu deinem neuen Job",
                  style: GoogleFonts.lato(
                    color: HexColor("#4A5568"),
                    fontSize: 40,
                  ),
                  textAlign: TextAlign.center,
                ));
          }
          return Container(
              height: MediaQuery.of(context).size.height * 0.10,
              width: MediaQuery.of(context).size.width * 0.20,
              child: AutoSizeText(
                "Drei einfache Schritte zu deinem neuen Job",
                style: GoogleFonts.lato(
                  color: HexColor("#4A5568"),
                  fontSize: 40,
                ),
                textAlign: TextAlign.center,
              ));
        }),
        Stack(
          alignment: Alignment.center,
          fit: StackFit.passthrough,
          children: [
            Column(
              children: [
                ServiceOneWeb(
                  number: 1,
                  title: "Erstellen dein Lebenslauf",
                ),
                Stack(
                  alignment: Alignment.center,
                  children: [
                    ClipPath(
                      clipper: MyServiceClipperWeb(),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.45,
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [
                              HexColor("#E6FFFA"),
                              HexColor("#EBF4FF"),
                            ],
                                stops: const [
                              0.0,
                              1.0
                            ],
                                begin: FractionalOffset.centerLeft,
                                end: FractionalOffset.centerRight,
                                tileMode: TileMode.repeated)),
                      ),
                    ),
                    ServiceTwoWeb(
                      number: 2,
                      title: "Erstellen dein Lebenslauf",
                    ),
                  ],
                ),
                ServiceThreeWeb(
                  number: 3,
                  title: "Mit nur einem Klick bewerben",
                ),
              ],
            ),
            CustomPaint(
              painter: CustomPainterWeb(),
              child: SizedBox(
                height: MediaQuery.of(context).size.height * 1.5,
                width: MediaQuery.of(context).size.width,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
