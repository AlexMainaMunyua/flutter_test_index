import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test_index/widgets/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class JumbotronMobile extends StatelessWidget {
  const JumbotronMobile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: MyJumboTronClipper(),
      child: Container(
        height: MediaQuery.of(context).size.height * 0.9,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
              HexColor("#EBF4FF"),
              HexColor("#E6FFFA"),
            ],
                stops: const [
              0.0,
              1.0
            ],
                begin: FractionalOffset.centerLeft,
                end: FractionalOffset.centerRight,
                tileMode: TileMode.repeated)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              // color: Colors.blue,
              alignment: Alignment.center,
              height: MediaQuery.of(context).size.height * 0.18,
              width: MediaQuery.of(context).size.width * 0.6,
              child: AutoSizeText(
                "Deine Job Website",
                style: GoogleFonts.lato(
                    letterSpacing: 2,
                    fontWeight: FontWeight.w600,
                    fontSize: 42),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.6,
              width: MediaQuery.of(context).size.width,
              child: SvgPicture.asset(
                "assets/images/jumbotron_one.svg",
                fit: BoxFit.cover,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
