import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test_index/widgets/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hexcolor/hexcolor.dart';

class JumbotronWeb extends StatelessWidget {
  const JumbotronWeb({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: MyJumboTronClipper(),
      child: Container(
        height: MediaQuery.of(context).size.height * 0.55,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
              HexColor("#EBF4FF"),
              HexColor("#E6FFFA"),
            ],
                stops: const [
              0.0,
              1.0
            ],
                begin: FractionalOffset.centerLeft,
                end: FractionalOffset.centerRight,
                tileMode: TileMode.repeated)),
        child: Row(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.22,
            ),
            Container(
              alignment: Alignment.centerLeft,
              height: MediaQuery.of(context).size.height * 0.45,
              width: MediaQuery.of(context).size.height * 0.35,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.15,
                    width: MediaQuery.of(context).size.height * 0.35,
                    child: AutoSizeText(
                      "Deine Job Website",
                      style: GoogleFonts.lato(fontSize: 65),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.width * 0.02,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      alignment: Alignment.centerLeft,
                      height: MediaQuery.of(context).size.height * 0.04,
                      width: MediaQuery.of(context).size.height * 0.3,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          gradient: LinearGradient(
                              colors: [
                                HexColor("#319795"),
                                HexColor("#3182CE"),
                              ],
                              stops: const [
                                0.0,
                                1.0
                              ],
                              begin: FractionalOffset.centerLeft,
                              end: FractionalOffset.centerRight,
                              tileMode: TileMode.repeated)),
                      child: Center(
                        child: AutoSizeText(
                          "Kostenlos Registrieren",
                          style: GoogleFonts.lato(
                              color: HexColor("#E6FFFA"), fontSize: 14),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.05,
            ),
            Container(
                child: CircleAvatar(
              backgroundColor: Colors.white,
              radius: MediaQuery.of(context).size.width * 0.1,
              child: SvgPicture.asset("assets/images/jumbotron_one.svg"),
            )),
          ],
        ),
      ),
    );
  }
}
